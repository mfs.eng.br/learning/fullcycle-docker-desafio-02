const express = require('express');
const app = express();
const port = 3000;
const config = {
    host: 'db',
    user: 'root',
    password: 'root',
    database: 'nodedb'
};
const mysql = require('mysql');
const connection = mysql.createConnection(config);

// Middleware
app.use(express.urlencoded({ extended: true }));


// Cria a tabela se não existir
const createTableSql = `
CREATE TABLE IF NOT EXISTS people(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL
)`;

connection.query(createTableSql, (error) => {
    if (error) throw error;
    console.log('Tabela verificada/criada com sucesso.');
});


connection.end();

app.get('/', (req, res) => {
    const connection = mysql.createConnection(config);
    const selectSql = `SELECT * FROM people`;
    connection.query(selectSql, (error, results) => {
        if (error) throw error;

        let html = '<h1>Full Cycle</h1>';
        html += '<form method="POST" action="/add">';
        html += '<input type="text" name="name" placeholder="Digite um nome...">';
        html += '<button type="submit">Adicionar</button>';
        html += '</form>';
        html += '<ul>';

        results.forEach(person => {
            html += `<li>${person.name}</li>`;
        });

        html += '</ul>';

        res.send(html);
        connection.end();
    });
});

// Rota POST para inserir um novo item
app.post('/add', (req, res) => {
    const connection = mysql.createConnection(config);
    const { name } = req.body;
    const insertSql = `INSERT INTO people(name) VALUES(?)`;
    connection.query(insertSql, [name], (error) => {
        if (error) throw error;
        res.redirect('/');
    });
    connection.end();
});



app.listen(port, () => {
    console.log('Rodando na porta ' + port);
});
